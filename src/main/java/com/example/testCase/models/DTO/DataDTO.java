package com.example.testCase.models.DTO;

import java.util.Date;

public class DataDTO {
  private int id ;
  private String nama ; 
  private String tempatLahir ;
  private Date tanggalLahir ;
  private String email ;
  private String telp ;
  private String alamat ;
  public DataDTO() {
  }
  public DataDTO(int id, String nama, String tempatLahir, Date tanggalLahir, String email, String telp, String alamat) {
    this.id = id;
    this.nama = nama;
    this.tempatLahir = tempatLahir;
    this.tanggalLahir = tanggalLahir;
    this.email = email;
    this.telp = telp;
    this.alamat = alamat;
  }
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getNama() {
    return nama;
  }
  public void setNama(String nama) {
    this.nama = nama;
  }
  public String getTempatLahir() {
    return tempatLahir;
  }
  public void setTempatLahir(String tempatLahir) {
    this.tempatLahir = tempatLahir;
  }
  public Date getTanggalLahir() {
    return tanggalLahir;
  }
  public void setTanggalLahir(Date tanggalLahir) {
    this.tanggalLahir = tanggalLahir;
  }
  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  public String getTelp() {
    return telp;
  }
  public void setTelp(String telp) {
    this.telp = telp;
  }
  public String getAlamat() {
    return alamat;
  }
  public void setAlamat(String alamat) {
    this.alamat = alamat;
  }
  
}
