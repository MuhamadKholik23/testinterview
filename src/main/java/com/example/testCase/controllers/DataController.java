package com.example.testCase.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.testCase.models.Data;
import com.example.testCase.models.DTO.DataDTO;
import com.example.testCase.repository.DataRepo;

@RestController
@RequestMapping("/api/dataTest")
public class DataController {
  @Autowired
	DataRepo dataRepo ;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> createBookRating(@Validated @RequestBody Data body){
		Map<String, Object> result = new HashMap<String, Object>();
		dataRepo.save(body);
		
		DataDTO dataDTO = modelMapper.map(body, DataDTO.class);
		result.put("message", "Success Create Order");
		result.put("data", dataDTO);
		return result;
	}
	
	@GetMapping("/readall")
	public Map<String, Object> getAll(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Data> listData =  dataRepo.findAll();
		List<DataDTO> listDataDTO = new ArrayList<DataDTO>();
		
		for (Data entity : listData) {
			DataDTO dataDTO = modelMapper.map(entity, DataDTO.class);
			
			listDataDTO.add(dataDTO);
		}
		
		result.put("message", "Read All Data Success");
		result.put("data", listDataDTO);
		result.put("total", listDataDTO.size());
		
		return result;
	}
	
	@PutMapping("/update")
	public Map<String, Object> updateBookRating(@Param("id") Long id, @Valid @RequestBody DataDTO body){
		Map<String, Object> result = new HashMap<String, Object>();
		Data data = dataRepo.findById(id).get();
		data = modelMapper.map(body, Data.class);
		dataRepo.save(data);
		result.put("message", "Success Update Data");
		return result;
	}
	
	@DeleteMapping("/delete/{id}")
    public Map<String, Object> deleteBookRating(@PathVariable(value = "id") Long BookRatingId) {
		Map<String, Object> result = new HashMap<String, Object>();
		Data data = dataRepo.findById(BookRatingId).get();
        
		dataRepo.delete(data);
        result.put("message", "Success delete data");
		return result;
    }
  
}
