package com.example.testCase.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.testCase.models.Data;

@Repository
public interface DataRepo extends JpaRepository<Data, Long>{

}

